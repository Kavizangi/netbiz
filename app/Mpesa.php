<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mpesa extends Model
{
    protected $table='mpesa_payments';

    protected $fillable=['mobile','username','transactionID','username','status','status_code',
        'status_message','amount','CheckoutRequestID','MerchantRequestID','query_response','listener_response','validate_response'];

}
