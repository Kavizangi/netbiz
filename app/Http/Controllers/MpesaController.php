<?php

namespace App\Http\Controllers;

use App\Mpesa;
use App\MPESA\MPESAPayments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class MpesaController extends Controller
{
    public function validateMpesaCallBack(Request $request){
        $data= $request->json()->all();

        $message = 'Mpesa VALIDATION CallBack from: '.url('/');
        $message.= json_encode($data);

        Log::error("validateMpesaCallBack >> ".$message);

        echo '{"ResultCode": 0, "ResultDesc": "Success"}';
    }

    public function mpesaStkCallBack(Request $request){
        $data=$request->json()->all();
        try{
            $Body = $data['Body'];
            $stkCallback = $Body['stkCallback'];
            $CheckoutRequestID = $stkCallback['CheckoutRequestID'];
            $ResultDesc = $stkCallback['ResultDesc'];

            $mpesa = Mpesa::where('CheckoutRequestID', $CheckoutRequestID)->first();
            if($mpesa){
                $mpesa->status_message = $ResultDesc;
                $mpesa->listener_response = json_encode($data);
                if($mpesa->result_code == 0){
                    $mpesa->status = 'successful';
                }else{
                    $mpesa->status = 'failed';
                }
                $mpesa->save();

            }else{
                Log::error("mpesaStkCallBack CheckoutRequestID not found ");
            }
        }catch (\Exception $ex){
            Log::error("mpesaStkCallBack Exception > ".$ex->getMessage());
        }
    }

    public function mpesaConfirmationCallBack(Request $request){
        $data=$request->json()->all();
        try{
            $BillRefNumber = $data['BillRefNumber'];
            $TransAmount = $data['TransAmount'];
            $TransID = $data['TransID'];
            $MSISDN = $data['MSISDN'];
            $FirstName = array_key_exists('FirstName', $data)?$data['FirstName']:'';
            $MiddleName = array_key_exists('MiddleName', $data)?$data['MiddleName']:'';
            $LastName = array_key_exists('LastName', $data)?$data['LastName']:'';
            $sender_name = $FirstName.' '.$MiddleName.' '.$LastName;

            $mpesa = new Mpesa();
            $mpesa->mobile = $MSISDN;
            $mpesa->transactionID = $TransID;
            $mpesa->amount = $TransAmount;
            $mpesa->names = $sender_name;
            $mpesa->save();

            Log::error("mpesaConfirmationCallBack >> ".json_encode($data));

        }catch (\Exception $ex){
            Log::error("mpesaConfirmationCallBack >> ".$ex->getMessage());
        }
    }

    public function registerURLs(){
        $mpesa = new MPESAPayments();
        $response = $mpesa->registerURLs();
        Log::info("registerURLs > ".json_encode($response));
    }

    public function mpesaWithdrawalCallBack(Request $request){
        $data=$request->json()->all();
        try{

            $Result = $data['Result'];
            $ResultCode = $Result['ResultCode'];
            $ResultDesc = $Result['ResultDesc'];
            $OriginatorConversationID = $Result['OriginatorConversationID'];
            $ConversationID = $Result['ConversationID'];
            $TransactionID = $Result['TransactionID'];

            $mpesa = Mpesa::where('CheckoutRequestID', $OriginatorConversationID)->first();
            if($mpesa){
                $mpesa->status_message = $ResultDesc;
                $mpesa->transactionId = $TransactionID;
                $mpesa->listener_response = json_encode($data);
                if($mpesa->result_code == 0){
                    $mpesa->status = 'successful';
                }else{
                    $mpesa->status = 'failed';
                }
                $mpesa->save();

            }else{
                Log::error("mpesaWithdrawalCallBack OriginatorConversationID not found ");
            }

        }catch (\Exception $ex){
            Log::error("mpesaWithdrawalCallBack Exception > ".$ex->getMessage());
        }
    }
}
