<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WithdrawalController extends Controller
{
    public function index(){
        $data['withdrawals'] = [];
        $data['withdrawal_total'] = 0;
        return view('customer.withdrawals',compact('data'));
    }
}
