<?php

namespace App\Http\Middleware;
use Closure;


class IsAdmin
{
    public function handle($request, Closure $next)
    {
        if($request->user() && $request->user()->super_admin == "y") {
            return $next($request);
        }
        return redirect(route('account.home'));
    }
}
