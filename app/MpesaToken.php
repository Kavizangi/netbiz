<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class MpesaToken extends Model
{
    protected $table='mpesa_token';
    protected $fillable=['access_token','expires_in','granted_at'];

    public function getToken(){
        $granted_at = $this->granted_at;
        $expires_in = $this->expires_in;

        try {
            $expiry_time = new Carbon($granted_at);
            $expiry_time->addSeconds($expires_in);

            $now = Carbon::now();
            $diff = $now->diffInSeconds($expiry_time, false);
            $min = 60;
            if($diff < $min){
                return null;
            }
        } catch (\Exception $e) {
            return null;
        }

        return $this->access_token;
    }
}
