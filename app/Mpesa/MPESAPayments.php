<?php

namespace App\MPESA;

use App\Mpesa;
use App\MpesaToken;
use Illuminate\Support\Carbon;

class MPESAPayments{

    private $config;
    private $access_token;
    private $publicKey;
    private $InitiatorName;
    private $InitiatorPassword;

    public function __construct()
    {
        // change this configurations
        $this->config = [
            'baseUrl'           => 'https://sandbox.safaricom.co.ke/',
            'CONSUMER_KEY'      => 'ToS1TEFMYuxGnl08hJAfD2o7KcMKbENQ',
            'CONSUMER_SECRET'   => 'DQLlI0Q72BTvoMM0',
            'PartyA'            => '174379',
            'CallBackURL'       => 'http://42e4355302ea.ngrok.io/listener',
            'passKey'           => 'bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919',
            'validationUrl'     => 'http://42e4355302ea.ngrok.io/validation',
            'confirmationUrl'   => 'http://42e4355302ea.ngrok.io/confirmation',
            'B2C_ResultURL'   => 'http://42e4355302ea.ngrok.io/withdrawal_listener'
        ];

        $this->initAccessToken();
        $publicKey=""; // http://mpesaintegration.24i.co.ke/53/
        $InitiatorName=""; // USER_NAME_FROM_MPESA_PORTAL_HERE
        $InitiatorPassword=""; //YOUR_PASSWORD_HERE
    }

    public function getToken(){

        $credentials = base64_encode($this->config['CONSUMER_KEY'].':'.$this->config['CONSUMER_SECRET']);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->config['baseUrl'].'oauth/v1/generate?grant_type=client_credentials');
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Basic '.$credentials));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $curl_response = curl_exec($curl);
        curl_close($curl);

        return (array) json_decode($curl_response);

    }

    public function initAccessToken()
    {
        $mpesa_token = MpesaToken::all()->first();
        if($mpesa_token){
            $token = $mpesa_token->getToken();
            if(!empty($token)){
                $this->access_token = $token;
                return $this->access_token;
            }
        }

        $response = $this->getToken();

        $response['granted_at'] = Carbon::now();
        $mpesa_token = MpesaToken::all()->first();
        if($mpesa_token){
            $mpesa_token->update($response);
        }else{
            MpesaToken::create($response);
        }
        $this->access_token = $response['access_token'];
        return $this->access_token;

    }

    protected function getCommonHeader(){
        $this->initAccessToken();
        return array('Content-Type: application/json','Authorization: Bearer ' . $this->access_token);
    }

    public function registerURLs(){
        $url = $this->config['baseUrl'].'/mpesa/c2b/v1/registerurl';
        $data = [];
        $data['ShortCode'] = $this->config['PartyA'];
        $data['ResponseType'] = 'application/json';
        $data['ValidationURL'] = $this->config['validationUrl'];
        $data['ConfirmationURL'] = $this->config['confirmationUrl'];
        $header = $this->getCommonHeader();
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $response = curl_exec($curl);
        return json_decode($response,true);
    }

    public function stkPush($amount,$mobile,$username,$desc, $resend=true){ //C2B

        $data = [];
        $timestamp=Carbon::now()->format('YmdHis');

        $data['BusinessShortCode']=$this->config['PartyA'];
        $data['Password']=base64_encode($this->config['PartyA'].$this->config['passKey'].$timestamp);
        $data["Timestamp"]=$timestamp;
        $data['TransactionType']='CustomerPayBillOnline';
        $data['Amount']=$amount;
        $data['PartyA']=$mobile;
        $data['PartyB']=$this->config['PartyA'];
        $data['PhoneNumber']=$mobile;
        $data['CallBackURL']=$this->config['CallBackURL'];
        $data['AccountReference']=$username;
        $data['TransactionDesc']=$desc;

        $url = $this->config['baseUrl']."/mpesa/stkpush/v1/processrequest";

        $header = $this->getCommonHeader();
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $curl_response = curl_exec($curl);
        $response = json_decode($curl_response,true);

        if($resend && is_array($response) && array_key_exists('errorCode', $response)){
            $errorCode = $response['errorCode'];
            if($errorCode == '404.001.03'){ //Invalid Access Token
                $mpesa = MpesaToken::all()->first();
                if($mpesa){
                    $mpesa->delete();
                    return $this->stkPush($amount, $mobile ,$username, $desc, false);
                }
            }
        }
        return $response;
    }

    public function sendStkRequest($mobile,$amount,$username,$description)
    {
        $amount = intval($amount);

        $data=[
            'username'=>$username,
            'mobile'=>$mobile,
            'status'=>'pending',
            'status_code'=>'400',
            'status_message'=>' Sent. Pending confirmation.',
            'amount'=>$amount,
        ];

        $request=Mpesa::create($data);
        $response=$this->stkPush($amount,$mobile,$username,$description);
        if($response == null){
            $response = array();
        }

        if(array_key_exists('MerchantRequestID',$response) &&  array_key_exists('CheckoutRequestID',$response)){
            $mpesa=Mpesa::find($request->id);
            $mpesa->MerchantRequestID=$response['MerchantRequestID'];
            $mpesa->CheckoutRequestID=$response['CheckoutRequestID'];
            $mpesa->query_response = json_encode($response);
            if($mpesa->response_code == 0){
                $mpesa->status = 'processing';
            }else{
                $mpesa->status = 'failed';
            }
            $mpesa->save();
            if($mpesa->response_code == 0){
                return $response['CheckoutRequestID'];
            }
        }
        return $response;

    }

    public function validateStkPush($checkout){
        $url=$this->config['baseUrl']."/mpesa/stkpushquery/v1/query";
        $timestamp= \Carbon\Carbon::now()->format('YmdHis');

        $data=[];
        $data['BusinessShortCode']=$this->config['PartyA'];
        $data['Password']=base64_encode($this->config['PartyA'].$this->config['passKey'].$timestamp);
        $data["Timestamp"]=$timestamp;
        $data['CheckoutRequestID']=$checkout;
        $header = $this->getCommonHeader();
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $response = curl_exec($curl);
        return json_decode($response,true);

    }

    public function withdraw($amount, $mobileNo, $resend=true) // B2C
    {
        openssl_public_encrypt($this->InitiatorPassword, $encrypted, $this->publicKey, OPENSSL_PKCS1_PADDING);
        $SecurityCredentials=base64_encode($encrypted);

        $data = [];
        $data['InitiatorName'] =$this->InitiatorName; //The username of the M-Pesa B2C account API operator.
        $data['CommandID'] ='BusinessPayment'; //	Unique command for each transaction type e.g. SalaryPayment, BusinessPayment, PromotionPayment
        $data['SecurityCredential']=$SecurityCredentials; //	Base64 encoded string of the B2C short code and password, which is encrypted using M-Pesa public key and validates the transaction on M-Pesa Core system.
        $data['QueueTimeOutURL'] = $this->config['B2C_ResultURL'];//The timeout end-point that receives a timeout response.
        $data['ResultURL'] = $this->config['B2C_ResultURL'];//The end-point that receives the response of the transaction
        $data['Amount'] = $amount;
        $data['PartyA'] = $this->config['PartyA'];//Organization’s shortcode initiating the transaction.
        $data['PartyB'] = $mobileNo;//Phone number receiving the transaction
        $data['Remarks'] = 'Withdrawal';//Any additional information to be associated with the transaction.(string)
        $data['Occasion'] = 'Withdrawal'; //Any additional information to be associated with the transaction.(Alpha-numeric)

        $url = $this->config['baseUrl']."/mpesa/b2c/v1/paymentrequest";
        $header = $this->getCommonHeader();
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $curl_response = curl_exec($curl);
        $response = json_decode($curl_response,true);

        if($resend && is_array($response) && array_key_exists('errorCode', $response)){
            $errorCode = $response['errorCode'];
            if($errorCode == '401.002.01'){ //Invalid Access Token
                $mpesa_token = MpesaToken::all()->first();
                if($mpesa_token){
                    $mpesa_token->delete();
                    return $this->withdraw($amount, $mobileNo,false);
                }
            }
        }
        return $response;
    }

}
