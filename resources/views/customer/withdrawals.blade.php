@extends('customer.layouts.main')

@section('title','Withdrawals')
@section('page_title','Withdrawals')

@section('content')


    <div class="row">

        <div class="col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Total Withdrawals</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ mCurrency($data['withdrawal_total']) }}</div>
                        </div>
                        <div class="col-auto">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
