@extends('admin.layouts.main')

@section('title','Payments')
@section('page_title','Payment Details')

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="text-primary text-uppercase float-right">Total Withdrawal is: {{ number_format(\App\Withdrawal::sum(('amount'))) }}</h4>
        </div>
        <div class="card-body">

        </div>
    </div>
@endsection
