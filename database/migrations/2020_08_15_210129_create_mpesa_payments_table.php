<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMpesaPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mpesa_payments', function (Blueprint $table) {
            $table->id();
            $table->string("mobile");
            $table->string("username")->nullable();
            $table->string("names")->nullable();
            $table->string("transactionID")->nullable();
            $table->string("status");
            $table->string("status_code");
            $table->string("status_message");
            $table->float("amount");
            $table->string("CheckoutRequestID")->nullable();
            $table->string("MerchantRequestID")->nullable();
            $table->string("listener_response")->nullable();
            $table->string("validate_response")->nullable();
            $table->string("query_response")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mpesa_payments');
    }
}
